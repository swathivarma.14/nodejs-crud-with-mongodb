# Nodejs CRUD with mongoDB

Perform CRUD operations using Nodejs and Express
Integrate mongoDB

## Getting started

First, run the development server:

```bash
node server.js
# or
npx nodemon server.js
```

```bash
install mongoDB compass

run the following folders
cd C:\Program Files\MongoDB\Server\3.6\bin
mongod.exe --dbpath C:Users\Swathi\mongo-data
```

Open [http://localhost:3000/employee](http://localhost:3000/employee) with your browser to see the result.

```
cd existing_repo
git remote add origin https://gitlab.com/swathivarma.14/nodejs-crud-with-mongodb.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/swathivarma.14/nodejs-crud-with-mongodb/-/settings/integrations)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## License
For open source projects, say how it is licensed.
